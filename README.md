Cosmology lecture slides
========================

(C) Boud Roukema 2012, 2014, 2020 GPL-3+

These scripts are free software: you can redistribute them and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

These scripts are distributed in the hope that they will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
Public License for more details. See <http://www.gnu.org/licenses/>.

Presentation pdf for 30-hour course in cosmology. All materials are
fully free-licensed. See the download URLs for details of individual
figures, some of which have more permissive conditions than the
default CC-BY-SA-4.0 licence.
    
You should be able to create the pdf with `make -f Cosmology.mk`,
although you may have to install some standard packages such
as:
* fig2dev (e.g. the package could be called `fig2dev` or `fig2ps`)
* ghostscript
* imagemagick
* texlive-latex-base
* texlive-latex-extra
* texlive-latex-recommended
* xfig
e.g. in a Debian GNU/Linux or Debian derivative system such as Ubuntu, do
````shell
sudo aptitude install fig2dev ghostscript imagemagick \
  texlive-latex-base texlive-latex-extra texlive-latex-recommended xfig
````

Imagemagick security policy:

You will quite likely have to temporarily modify your imagemagick
security policy, which will need sudo (root) access. Type
`convert -list policy` to find your current policy, and temporarily add
necessary permissions if needed. See
https://imagemagick.org/script/security-policy.php for details.
Remember to restore your original security policy afterwards.
