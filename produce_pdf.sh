#!/usr/bin/env bash

## (c) Boud Roukema 2012, 2014, 2020 GPL-3+
##
## Function(s) to produce clickable pdf for Cosmology lecture slides

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

# produce the .pdf file
# WARNING: the intermediate postscript files may be big, so
# make sure your have plenty of space in ${BIG_TMP_DIR}
function produce_pdf {
    set -e

    origdir=$(pwd)
    BASE_NAME=${1}
    TMP_DIR=${2}
    PDF_FINAL=${3}.pdf

    ## Create directory, copy LaTeX source there, and cd there
    mkdir -p ${TMP_DIR}/tex/
    cp -pv ${BASE_NAME}.tex ${TMP_DIR}/tex/
    cd ${TMP_DIR}/tex/

    ## Symbolically link .eps files if not yet done
    ln -s ../non_svg/*.eps . || true
    ln -s ../svg_only/*.eps . || true
    ln -s ../fig_svg/*.eps . || true

    ## Compile the file
    latex ${BASE_NAME}.tex
    latex ${BASE_NAME}.tex
    #xelatex ${BASE_NAME}.tex && xelatex ${BASE_NAME}.tex
    dvips ${BASE_NAME}.dvi -o ${BASE_NAME}.ps
    printf "\n\n\n========== Will run ps2pdf... ============\n\n\n"
    # 2022-01-31 added -dALLOWPSTRANSPARENCY
    #https://github.com/GenericMappingTools/gmt/issues/4453
    # May need the following as a temporary hack around 2022/2023:
    sed -e 's/\.setopacityalpha/.setfillconstantalpha/g' ${BASE_NAME}.ps > ${BASE_NAME}.eps
    # See https://ghostscript.readthedocs.io/en/gs10.0.0/Use.html for -dNORANGEPAGESIZE
    ps2pdf -dALLOWPSTRANSPARENCY -dNORANGEPAGESIZE ${BASE_NAME}.eps ${BASE_NAME}.pdf

    ## Tidy up
    cd ${origdir}
    cp -pv ${TMP_DIR}/tex/${BASE_NAME}.pdf ${PDF_FINAL}
    echo " "
    echo "You should now have the pdf file:"
    ls -l ${PDF_FINAL}
    echo "You should keep this file." #${FILES_ANIMATIONS}
    echo " "
    echo "The other files can be safely removed, including those"
    echo "in ${TMP_DIR}/, with 'make -f Cosmology.mk clean' ."
    ls -l ${TMP_DIR}/*
    echo "However, to avoid wasted downloads, you make keep them in place."
}

set -e

produce_pdf $1 $2 $3
