#!/usr/bin/env bash

## (c) Boud Roukema 2012, 2014, 2020 GPL-3+
##
## Download functions for Cosmology lecture slides

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


## files available from WMCommons in .svg format that do not have
## source code available for producing them; convert locally by brute force
## * input file names must include desired px size:  148px-Commons-logo-en.svg
function download_svg_only {
    TMP_DIR=${1}/svg_only/
    FILES=${2}
    ## URLs for downloaded full html pages, pixel sizes removed from string
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=$(echo ${FILES} | sed -e "s,[0-9]\+px-\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g")

    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}

    ## store pixel sizes and html file names together in each string
    FILES_COLON=$(echo ${FILES} | sed -e 's,\([0-9]\+\)px-\([^ ]*\),\1:::File\:\2,g')

    for file_colon in ${FILES_COLON}; do
        ## html file name that should now be locally available
        FILE=$(echo ${file_colon} |sed -e 's/^.*::://')
        URL=$(grep -i "fileInfo.*SVG file" ${FILE} | sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},https:${DSLASH},")
        sleep 2
        wget --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=$(echo ${FILE} |sed -e 's/^File://')
        PIXEL_WIDTH=$(echo ${file_colon}|sed -e 's/:::.*$//')
        FILE_BASE=$(echo ${FILE_ORIG}|sed -e 's/\.svg//')

        ## produce a brute-force .eps figure using 'convert' from imagemagick
        convert ${FILE_ORIG} -resize ${PIXEL_WIDTH} ${PIXEL_WIDTH}px-${FILE_BASE}.eps

        echo "The following eps file should now exist:"
        ls -l ${PIXEL_WIDTH}px-${FILE_BASE}.eps
        if [ $(cat ${PIXEL_WIDTH}px-${FILE_BASE}.eps | wc -c) -eq 0 ]; then
            printf "Error: ${PWD}/${PIXEL_WIDTH}px-${FILE_BASE}.eps is 0 bytes long; check for errors.\n"
            printf "You may need to temporarily override imagemagick security;\n"
            printf "See https://imagemagick.org/script/security-policy.php\n"
            exit 1
        fi
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}

download_svg_only $1 $2
