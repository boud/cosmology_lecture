#!/usr/bin/env bash

## (c) Boud Roukema 2012, 2014, 2020 GPL-3+
##
## Download functions for Cosmology lecture slides

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

function download_fig_svg {
    TMP_DIR=${1}/fig_svg/
    FILES=${2}
    printf "download_fig_svg: parameters: _$1_ _$2_\n"

    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=$(echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g")
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    FILES_COLON=$(echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g')

    TMP_FILE=tmp_xx
    for file in ${FILES_COLON}; do
        ## extract .fig source
        csplit -f ${TMP_FILE} ${file} '/<.*pre>/' '{1}'
        sed -e 's/<pre>//' ${TMP_FILE}01 | \
            sed -e 's/&gt;/>/g' -e 's/&lt;/</' \
                -e 's/&#160;/ /g' \
            >  ${TMP_FILE}.fig

        ## produce a reasonable size .eps figure
        fig2dev -L pstex -F ${TMP_FILE}.fig > ${TMP_FILE}.pstex
        fig2dev -L pstex_t -p ${TMP_FILE}.pstex ${TMP_FILE}.fig > ${TMP_FILE}.pstex_t

        ## start a latex-able file
        cat >${TMP_FILE}.tex <<EOF
\documentclass[12pt]{article}
\usepackage{graphicx,color}
\pagestyle{empty}
\begin{document}
EOF

        echo "\\input{" ${TMP_FILE}.pstex_t "}" >> ${TMP_FILE}.tex

        cat >> ${TMP_FILE}.tex <<EOF
\end{document}
EOF

        latex ${TMP_FILE}.tex
        dvips ${TMP_FILE}.dvi -E -o ${TMP_FILE}.eps

        ## restore file names
        FILE_BASE=$(echo ${file} |sed -e 's/\.svg//'|sed -e 's/File://')

        mv ${TMP_FILE}.fig ${FILE_BASE}.fig
        mv ${TMP_FILE}.eps ${FILE_BASE}.eps
        printf "The following eps file should now exist:\n"
        ls -l ${FILE_BASE}.eps
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        rm ${TMP_FILE}0[012]
        for i in aux dvi log pstex pstex_t tex; do
            rm ${TMP_FILE}.${i}
        done
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}

download_fig_svg $1 $2
