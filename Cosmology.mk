## (c) Boud Roukema 2012, 2014, 2020 GPL-3+
##
## Cosmology lecture slides

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

.ONESHELL:
.SHELLFLAGS = -ec

datestamp-file = datestamp
texsrc = cosmology
pdfname = Cosmology_30h_lecture
build-dir = .build

output-pdf = $(pdfname).pdf

LANG = en_US.UTF-8
LANGUAGE = en:en_US
LC_NUMERIC = C

## Not needed for this course:
#FILES_OCTAVE=`for i in x y r theta theta_quadrant; do echo Scalar_fields_${i}.svg; done`
#files-octave-base = x y r theta theta_quadrant
#files-octave = $(patsubst %, Scalar_fields_%.svg, $(files-octave-base))

## Not needed for this course:
#files-fig-svg = Rotation_Euclidean1.svg Rotation_Euclidean2.svg	\
#Rotation_Euclidean_basis.svg Vector_basis_polar_coordinates.svg	\
#One-form_basis_polar_coordinates.svg

files-fig-svg-base = Pythagz.svg Pythagm.svg Pythagp.svg		\
                      Cosmic_topology_1_intuitive.svg                 	\
                      Spherical_arc.svg Hyperbolic_ar.svg

files-fig-svg = $(subst .svg,.eps,$(patsubst %, $(build-dir)/fig_svg/%, $(files-fig-svg-base)))

#FILES_WIKIBOOKS="NONE"

files-png-base = Aleksandr_Fridman.png Universe_expansion.png	\
                 CosmoDistanceMeasures_z_to_onehalf.png		\
                 CosmoDistanceMeasures_z_to_1e4.png

files-jpg-base = Lemaitre.jpg End_of_universe.jpg		\
                 Azimuthal_equidistant_projection_SW.jpg		\
                 Schwarzschild.jpg Orthographic_projection_SW.jpg 	\
                 Firas_spectrum.jpg

files-gif-base = COBE_monopole_dipole_and_primordial_perturbations.gif


files-png = $(subst .png,.eps,$(patsubst %, $(build-dir)/non_svg/%, $(files-png-base)))

files-jpg = $(subst .jpg,.eps,$(patsubst %, $(build-dir)/non_svg/%, $(files-jpg-base)))

files-gif = $(subst .gif,.eps,$(patsubst %, $(build-dir)/non_svg/%, $(files-gif-base)))

## copyvios - removed from Wikimedia Commons:
## The_main_nuclear_reaction_chains_for_Big_Bang_nucleosynthesis.jpg

## 2020-09-30 - some imagemagick/svg installations can't handle url("#pattern_grid_1")
## 500px-Nucleosynthesis_periodic_table.svg

##TODO: UPDATE
files-svg-only-base = 500px-Friedmann_universes_bold.svg \
                       500px-Cosmic_topology_2_fundamental_domain.svg \
                       500px-Cosmic_topology_3_apparent_space.svg

files-svg-only = $(subst .svg,.eps,$(patsubst %, $(build-dir)/svg_only/%, $(files-svg-only-base)))

file-pdf = $(pdfname).pdf


#$(files-octave): $(build-dir)
#	printf "Will try to get and build $@ ...\n"
#	./download_octave.sh $(build-dir) $@

all: $(files-png) $(files-jpg) $(files-svg-only) $(files-fig-svg) \
       $(files-gif) \
       $(output-pdf)

#$(licences-checked)

$(files-png): | $(build-dir)
	file_non_svg=$(subst .eps,.png,$@)
	printf "Will try to download $${file_non_svg} to create $@ ...\n"
	./download_non_svg.sh $(build-dir) $$(basename $${file_non_svg})

$(files-jpg): | $(build-dir)
	file_non_svg=$(subst .eps,.jpg,$@)
	printf "Will try to download $${file_non_svg} to create $@ ...\n"
	./download_non_svg.sh $(build-dir) $$(basename $${file_non_svg})

$(files-gif): | $(build-dir)
	file_non_svg=$(subst .eps,.gif,$@)
	printf "Will try to download $${file_non_svg} to create $@ ...\n"
	./download_non_svg.sh $(build-dir) $$(basename $${file_non_svg})

$(files-svg-only): | $(build-dir)
	file_svg_only=$(subst .eps,.svg,$@)
	printf "Will try to download $${file_svg_only} to create $@ ...\n"
	./download_svg_only.sh $(build-dir) $$(basename $${file_svg_only})

$(files-fig-svg): | $(build-dir)
	file_fig_svg=$(subst .eps,.svg,$@)
	printf "Will try to download $${file_fig_svg} to create $@ ...\n"
	./download_fig_svg.sh $(build-dir) $$(basename $${file_fig_svg})



$(output-pdf): cosmology.tex | $(build-dir)
	./produce_pdf.sh $(texsrc) $(build-dir) $(pdfname)


$(licences-checked):
	./check_licences.sh ## prior to uploading a new .pdf


$(datestamp-file):
	date=$$(date +%y%m%d)
	printf "$${date}" > $@

$(build-dir):
	mkdir -pv $@


clean:
	rm -fv $(datestamp-file)
	rm -frv .build/
