#!/usr/bin/env bash

## (c) Boud Roukema 2012, 2014, 2020 GPL-3+
##
## Download functions for Cosmology lecture slides

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## files available from Commons in various non svg formats
function download_non_svg {
    TMP_DIR=${1}/non_svg/
    FILENAME=${2}
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILE_HTML=$(echo ${FILENAME} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g")

    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILE_HTML}
    FILE_COLON=$(echo ${FILENAME} | sed -e 's,\([^ ]*\),File\:\1,g')

    for file in ${FILE_COLON}; do
        URL=$(grep fullMedia ${file} | sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},http:${DSLASH},")
        sleep 2
        wget -O ${FILENAME} --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=$(echo ${file} |sed -e 's/^File://')
        FILE_BASE=$(echo ${FILE_ORIG}|sed -e 's/\.[^\.]*$//')

        ## produce a brute-force .eps figure
        convert ${FILE_ORIG} ${FILE_BASE}.eps

        echo "The following eps file should now exist:"
        ls -l ${FILE_BASE}.eps
        if [ $(cat ${FILE_BASE}.eps | wc -c) -eq 0 ]; then
            printf "Error: ${PWD}/${FILE_BASE}.eps is 0 bytes long; check for errors.\n"
            printf "You may need to temporarily override imagemagick security;\n"
            printf "See https://imagemagick.org/script/security-policy.php\n"
            exit 1
        fi
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
        #printf "sleep 20..." ; sleep 20 ; printf "\n"
    done

}


download_non_svg $1 $2
